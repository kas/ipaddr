#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-
"""
This is a thin wrapper around Python's ipaddress module.

Usage: ip.x [OPTIONS] [IP_ADDRESS [IP_ADDRESS …]]

positional arguments:
  IP_ADDRESS       IPv4 or IPv6 address, or integer

optional arguments:
  -h, --help       show this help message and exit
  -V, --version    show version information and exit
  -C, --copyright  show copying policy and exit

The program takes one or more IP addresses as its arguments(s) and
prints the result on standard output.

If no addresses are provided on the commandline, the program will
read from standard input.
"""
##############################################################################
# This program is free software; you can redistribute it and/or modify it    #
# under the terms of the GNU General Public License as published by the Free #
# Software Foundation; either version 3 of the License, or (at your option)  #
# any later version.                                                         #
#                                                                            #
# This program is distributed in the hope that it will be useful, but with-  #
# out any warranty; without even the implied warranty of merchantability or  #
# fitness for a particular purpose. See the GNU General Public License for   #
# more details.  <http://gplv3.fsf.org/>                                     #
##############################################################################

import argparse
import ipaddress
import pathlib
import sys
from typing import Union

__author__ = 'Klaus Alexander Seistrup <klaus@seistrup.dk>'
__revision__ = '2022-12-08'
__version__ = f'0.0.9 ({__revision__})'
__copyright__ = f"""
ip.x {__version__}

Copyright © 2016-22 Klaus Alexander Seistrup <klaus@seistrup.dk>

This is free software; see the source for copying conditions. There is no
warranty; not even for merchantability or fitness for a particular purpose.
""".strip()

EPILOG = """
The program takes one or more IP addresses as its arguments(s)
and prints the result on standard output.

If no addresses are provided on the commandline, the program
will read from standard input.
""".strip()

ErrorLike = Union[Exception, str]
IPAddress = Union[ipaddress.IPv4Address, ipaddress.IPv6Address]


def die(reason: ErrorLike = '') -> None:
    """Exit gracefully."""
    if reason:
        print(reason, file=sys.stderr)
    sys.exit(1 if reason else 0)


def main(progname: str = 'ip.x') -> int:
    """Enter program here.

    Is that ”imperative mood” enough for you, flake8?
    """
    parser = argparse.ArgumentParser(
        prog=progname,
        formatter_class=argparse.RawTextHelpFormatter,
        epilog=EPILOG
    )
    parser.add_argument(
        '-V',
        '--version',
        action='version',
        version=f'%(prog)s/ip.x {__version__}',
        help='show version information and exit'
    )
    parser.add_argument(
        '-C',
        '--copyright',
        action='version',
        version=__copyright__,
        help='show copying policy and exit'
    )
    parser.add_argument(
        'IP_ADDRESS',
        nargs='*',
        help='IPv4 or IPv6 address'
    )
    args = parser.parse_args()

    methods = {
        "ip.compressed": lambda ip: ip.compressed,
        "ip.exploded": lambda ip: ip.exploded,
        "ip.ipv4_mapped": lambda ip: ip.ipv4_mapped,
        "ip.max_prefixlen": lambda ip: ip.max_prefixlen,
        "ip.packed": lambda ip: ip.packed.hex(),
        "ip.rptr": lambda ip: ip.reverse_pointer,
        "ip.sixtofour": lambda ip: ip.sixtofour,
        "ip.version": lambda ip: ip.version,
    }

    if (transform := methods.get(progname)) is None:
        print('Please call me as one of:', file=sys.stderr)
        for method in sorted(m for m in methods):
            print(f' · {method}', file=sys.stderr)
        sys.exit(1)

    try:
        for addr in map(lambda s: s.strip(), args.IP_ADDRESS or sys.stdin):
            if not addr:
                continue

            ipvx = ipaddress.ip_address(int(addr) if addr.isdigit() else addr)

            if (result := transform(ipvx)) is None:
                continue

            print(result)
    except (AttributeError, ValueError) as error:
        die(error)
    except (KeyboardInterrupt, BrokenPipeError):
        die()
    except (IOError, OSError, MemoryError) as error:
        die(error)

    return 0


if __name__ == '__main__':
    sys.exit(main(pathlib.Path(sys.argv[0]).name))

# eof
